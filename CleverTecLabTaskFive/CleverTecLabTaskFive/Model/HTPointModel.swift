//
//  HTPointModel.swift
//  CleverTecLabTaskFive
//
//  Created by Zakhar Klochkov on 14.10.21.
//

import Foundation

struct Point {
    var type: String
    var x: String
    var y: String
    var distanceToCenter: Double
}
