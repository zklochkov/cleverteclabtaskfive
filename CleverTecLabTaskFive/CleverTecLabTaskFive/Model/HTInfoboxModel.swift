//
//  HTInfoboxModel.swift
//  CleverTecLabTaskFive
//
//  Created by Zakhar Klochkov on 14.10.21.
//

import Foundation

struct InfoboxApiResponse {
    let infoboxes: [Infobox]
}

extension InfoboxApiResponse: Decodable {
    
    private enum InfoboxApiResponseCodingKeys: String, CodingKey {
        case infoboxes = "results"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: InfoboxApiResponseCodingKeys.self)
        infoboxes = try container.decode([Infobox].self, forKey: .infoboxes)
    }
}

struct Infobox {
    
    let area: String // - область
    let city_type: String // - тип населённого пункта
    let city: String // - название населённого пункта
    let address_type: String // - тип улицы
    let address: String // - название улицы
    let house: String // - дом (с корпусом)
    let install_place: String // - место установки
    let location_name_desc: String // - пояснение места установки
    let work_time: String // - режим работы инфокиоска
    let time_long: String // - режим работы инфокиоска (строка с разбивкой по дням)
    let gps_x: String // - координата широты
    let gps_y: String // - координата долготы
    let currency: String // - перечень валют с которыми работает инфокиоск
    let inf_type: String // - тип инфокиоска (Внутренний / Внешний)
    let cash_in_exist: String // - наличие купюроприемника (да / нет)
    let cash_in: String // - исправность купюроприемника (да / нет)
    let type_cash_in: String // - приёмник пачек банкнот (да / нет)
    let inf_printer: String // - возможность печати чека (исправность чекового принтера) (да / нет)
    let region_platej: String // - наличие платежа "Региональные платежи" (да / нет)
    let popolnenie_platej: String // - наличие платежа "Пополнение картсчета наличными" (да / нет)
    let inf_status: String // - исправность инфокиоска (да / нет)
}

extension Infobox: Decodable {
    
    enum InfoboxCodingKeys: String, CodingKey {
        
        case area
        case citytype = "city_type"
        case city
        case addresstype = "address_type"
        case address
        case house
        case installplace = "install_place"
        case locationnamedesc = "location_name_desc"
        case worktime = "work_time"
        case timelong = "time_long"
        case gpsx = "gps_x"
        case gpsy = "gps_y"
        case currency
        case inftype = "inf_type"
        case cashinexist = "cash_in_exist"
        case cashin = "cash_in"
        case typecashin = "type_cash_in"
        case infprinter = "inf_printer"
        case regionplatej = "region_platej"
        case popolnenieplatej = "popolnenie_platej"
        case infstatus = "inf_status"
    }
    
    
    init(from decoder: Decoder) throws {
        let infoboxContainer = try decoder.container(keyedBy: InfoboxCodingKeys.self)
        
        area = try infoboxContainer.decode(String.self, forKey: .area)
        city_type = try infoboxContainer.decode(String.self, forKey: .citytype)
        city = try infoboxContainer.decode(String.self, forKey: .city)
        address_type  = try infoboxContainer.decode(String.self, forKey: .addresstype)
        address = try infoboxContainer.decode(String.self, forKey: .address)
        house = try infoboxContainer.decode(String.self, forKey: .house)
        install_place = try infoboxContainer.decode(String.self, forKey: .installplace)
        location_name_desc = try infoboxContainer.decode(String.self, forKey: .locationnamedesc)
        work_time = try infoboxContainer.decode(String.self, forKey: .worktime)
        time_long = try infoboxContainer.decode(String.self, forKey: .timelong)
        gps_x = try infoboxContainer.decode(String.self, forKey: .gpsx)
        gps_y = try infoboxContainer.decode(String.self, forKey: .gpsy)
        currency = try infoboxContainer.decode(String.self, forKey: .currency)
        inf_type  = try infoboxContainer.decode(String.self, forKey: .inftype)
        cash_in_exist = try infoboxContainer.decode(String.self, forKey: .cashinexist)
        cash_in = try infoboxContainer.decode(String.self, forKey: .cashin)
        type_cash_in = try infoboxContainer.decode(String.self, forKey: .typecashin)
        inf_printer = try infoboxContainer.decode(String.self, forKey: .infprinter)
        region_platej = try infoboxContainer.decode(String.self, forKey: .regionplatej)
        popolnenie_platej = try infoboxContainer.decode(String.self, forKey: .popolnenieplatej)
        inf_status  = try infoboxContainer.decode(String.self, forKey: .infstatus)
    }
}

