//
//  HTATMModel.swift
//  CleverTecLabTaskFive
//
//  Created by Zakhar Klochkov on 14.10.21.
//

import Foundation

struct ATMApiResponse {
    let ATMs: [ATM]
}

extension ATMApiResponse: Decodable {
    
    private enum ATMApiResponseCodingKeys: String, CodingKey {
        case ATMs = "results"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ATMApiResponseCodingKeys.self)
        ATMs = try container.decode([ATM].self, forKey: .ATMs)
    }
}

struct ATM {
    
    let area: String // - область
    let city_type: String // - тип населённого пункта
    let city: String // - название населённого пункта
    let address_type: String // - тип улицы
    let address: String // - название улицы
    let house: String // - дом (с корпусом)
    let install_place: String // - место установки
    let work_time: String // - режим работы банкомата
    let gps_x: String // - координата широты
    let gps_y: String // - координата долготы
    let install_place_full: String // - пояснение места установки
    let work_time_full: String // - режим работы банкомата (строка с разбивкой по дням)
    let ATM_type: String // - тип банкомата (Внутренний / Внешний)
    let ATM_error: String // - исправность банкомата (да / нет)
    let currency: String // - выдаваемая валюта (BYN / USD / EUR)
    let cash_in: String // - наличие купюроприемника (да / нет)
    let ATM_printer: String // - возможность печати чека (исправность чекового принтера) (да / нет)
}

extension ATM: Decodable {
    
    enum ATMCodingKeys: String, CodingKey {
        
        case area
        case citytype = "city_type"
        case city
        case addresstype = "address_type"
        case address
        case house
        case installplace = "install_place"
        case worktime = "work_time"
        case gpsx = "gps_x"
        case gpsy = "gps_y"
        case installplacefull = "install_place_full"
        case worktimefull = "work_time_full"
        case ATMtype = "ATM_type"
        case ATMerror = "ATM_error"
        case currency
        case cashin = "cash_in"
        case ATMprinter = "ATM_printer"
    }
    
    
    init(from decoder: Decoder) throws {
        let atmContainer = try decoder.container(keyedBy: ATMCodingKeys.self)
        
        area = try atmContainer.decode(String.self, forKey: .area)
        city_type  = try atmContainer.decode(String.self, forKey: .citytype)
        city  = try atmContainer.decode(String.self, forKey: .city)
        address_type = try atmContainer.decode(String.self, forKey: .addresstype)
        address  = try atmContainer.decode(String.self, forKey: .address)
        house  = try atmContainer.decode(String.self, forKey: .house)
        install_place  = try atmContainer.decode(String.self, forKey: .installplace)
        work_time = try atmContainer.decode(String.self, forKey: .worktime)
        gps_x  = try atmContainer.decode(String.self, forKey: .gpsx)
        gps_y  = try atmContainer.decode(String.self, forKey: .gpsy)
        install_place_full = try atmContainer.decode(String.self, forKey: .installplacefull)
        work_time_full = try atmContainer.decode(String.self, forKey: .worktimefull)
        ATM_type = try atmContainer.decode(String.self, forKey: .ATMtype)
        ATM_error  = try atmContainer.decode(String.self, forKey: .ATMerror)
        currency = try atmContainer.decode(String.self, forKey: .currency)
        cash_in = try atmContainer.decode(String.self, forKey: .cashin)
        ATM_printer = try atmContainer.decode(String.self, forKey: .ATMprinter)
    }
}
