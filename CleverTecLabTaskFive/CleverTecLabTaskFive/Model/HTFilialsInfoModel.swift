//
//  HTFilialsInfoModel.swift
//  CleverTecLabTaskFive
//
//  Created by Zakhar Klochkov on 14.10.21.
//

import Foundation

struct FilialsInfoApiResponse {
    let filials: [Filial]
}

extension FilialsInfoApiResponse: Decodable {
    
    private enum FilialsInfoApiResponseCodingKeys: String, CodingKey {
        case filials = "results"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: FilialsInfoApiResponseCodingKeys.self)
        filials = try container.decode([Filial].self, forKey: .filials)
    }
}

struct Filial {
    
    let filial_id: String // - идентификационный номер
    let sap_id: String
    let filial_name: String // - название структурного подразделения
    let filial_num: String // - номер филиала структурного подразделения
    let cbu_num: String // - номер ЦБУ структурного подразделения
    let otd_num: String // - номер структурного подразделения
    let name_type: String // - тип населенного пункта
    let name: String // - название населенного пункта
    let street_type: String // - тип улицы
    let street: String // - название улицы
    let home_number: String // - номер дома (с корпусом)
    let name_type_prev: String // - тип населенного пункта (для временного месторасположения подразделения)
    let name_prev: String // - название населенного пункта (для временного месторасположения подразделения)
    let street_type_prev: String // - тип улицы (для временного месторасположения подразделения)
    let street_prev: String // - название улицы (для временного месторасположения подразделения)
    let home_number_prev: String // - номер дома (с корпусом) (для временного месторасположения подразделения)
    let info_text: String // - дополнительная информация
    let info_worktime: String // - время работы структурного подразделения
    let info_bank_bik: String // - БИК
    let info_bank_unp: String // - УНП
    let GPS_X: String // - координата широты
    let GPS_Y: String // - координата долготы
    let bel_number_schet: String // - № расчетного счета (в белорусских рублях) (для разовых перечислений на счета, к которым оформлены карточки физических лиц)
    let foreign_number_schet: String // - № расчетного счета (в иностранной валюте)  (для разовых перечислений на счета, к которым оформлены карточки физических лиц)
    let phone_info: String // - номер телефона с кодом
    let info_weekend1_day: String
    let info_weekend2_day: String
    let info_weekend3_day: String
    let info_weekend4_day: String
    let info_weekend5_day: String
    let info_weekend6_day: String
    let info_weekend7_day: String
    let info_weekend1_time: String
    let info_weekend2_time: String
    let info_weekend3_time: String
    let info_weekend4_time: String
    let info_weekend5_time: String
    let info_weekend6_time: String
    let info_weekend7_time: String
    let dop_num: String
    let strahovanie_med_rashodov: String
    let usl_broker: String
    let usl_buy_slitki: String
    let usl_card_internet: String
    let usl_cennie_bumagi: String
    let usl_check_dover_vnebanka: String
    let usl_cheki_gilie: String
    let usl_cheki_imuschestvo: String
    let usl_club_barhat: String
    let usl_club_kartblansh: String
    let usl_club_ledi: String
    let usl_club_nastart: String
    let usl_club_persona: String
    let usl_club_schodry: String
    let usl_coins_exchange: String
    let usl_depositariy: String
    let usl_dep_doverennosti: String
    let usl_dep_scheta: String
    let usl_dep_viplati: String
    let usl_docObligac_belarusbank: String
    let usl_dover_upr: String
    let usl_dover_upr_gos: String
    let usl_drag_metal: String
    let usl_ibank: String
    let usl_inkasso_priem: String
    let usl_inkasso_priem_deneg_bel: String
    let usl_inkasso_vyplata: String
    let usl_int_cards: String
    let usl_izbiz_scheta_operacii: String
    let usl_izbiz_scheta_otkr: String
    let usl_kamni_brill: String
    let usl_kompleksny_product: String
    let usl_konversiya_foreign_val: String
    let usl_loterei: String
    let usl_mo_rb: String
    let usl_operations_bezdokumentar_obligacii: String
    let usl_operations_po_schetam_belpochta: String
    let usl_operations_sber_sertif: String
    let usl_perechislenie_po_rekvizitam_kartochki: String
    let usl_plategi: String
    let usl_podlinnost_banknot: String
    let usl_pogashenie_documentar_obligacii: String
    let usl_popolnenieSchetaBezKart: String
    let usl_popolnenieSchetaBynIspKarts: String
    let usl_popolnenieSchetaUsdIspKarts: String
    let usl_pov: String
    let usl_priemDocPokupkaObl: String
    let usl_priem_cennostei_na_hranenie: String
    let usl_priem_cennostej_na_hranenie: String
    let usl_priem_docs_fl_depozit_operations: String
    let usl_priem_docs_vidacha_sopr_lgot_ipotech: String
    let usl_priem_doc_na_kredits_magnit: String
    let usl_priem_doc_na_kredits_overdrafts: String
    let usl_priem_doc_na_lizing: String
    let usl_priem_inkasso: String
    let usl_priem_obl_mf: String
    let usl_priem_platejei_byn_ip: String
    let usl_priem_platejei_eur_ip: String
    let usl_priem_zayvleniy_obsluzhivanie_derzhatelej: String
    let usl_prodaga_monet: String
    let usl_pupil_card: String
    let usl_razmen_foreign_val: String
    let usl_razm_prodazha_documentar_obligacii: String
    let usl_rb_card: String
    let usl_registration_val_dogovor: String
    let usl_return_BynIspKarts: String
    let usl_return_UsdIspKarts: String
    let usl_rko: String
    let usl_seif: String
    let usl_strahovanie_avto: String
    let usl_strahovanie_avto_pogran: String
    let usl_strahovanie_detei: String
    let usl_strahovanie_dohod_pod_zaschitoy: String
    let usl_strahovanie_express: String
    let usl_strahovanie_green_karta: String
    let usl_strahovanie_home: String
    let usl_strahovanie_kartochki: String
    let usl_strahovanie_kasko: String
    let usl_strahovanie_komplex: String
    let usl_strahovanie_medicine_nerezident: String
    let usl_strahovanie_perevozki: String
    let usl_strahovanie_s_zabotoi_o_blizkih: String
    let usl_strahovanie_timeAbroad: String
    let usl_strahovanie_zashhita_ot_kleshha: String
    let usl_strahovka_site: String
    let usl_striz: String
    let usl_stroysber: String
    let usl_stroysber_new: String
    let usl_subsidiya_scheta: String
    let usl_swift: String
    let usl_vklad: String
    let usl_vozvrat_nds: String
    let usl_vydacha_nal_v_banke: String
    let usl_vydacha_vypiski: String
    let usl_vypllata_bel_rub: String
    let usl_vzk: String
    let usl_wu: String
    let usl_plategi_all: String
    let usl_plategi_in_foreign_val: String
    let usl_plategi_za_proezd_v_polzu_banka: String
    let usl_plategi_minus_mobi: String
    let usl_plategi_minus_internet: String
    let usl_plategi_minus_mobi_internet_full: String
    let usl_plategi_nal_minus_krome_kredit: String
}

extension Filial: Decodable {
    
    enum FilialCodingKeys: String, CodingKey {
        
        case filialid = "filial_id"
        case sapid = "sap_id"
        case filialname = "filial_name"
        case filialnum = "filial_num"
        case cbunum = "cbu_num"
        case otdnum = "otd_num"
        case nametype = "name_type"
        case name
        case streettype = "street_type"
        case street
        case homenumber = "home_number"
        case nametypeprev = "name_type_prev"
        case nameprev = "name_prev"
        case streettypeprev = "street_type_prev"
        case streetprev = "street_prev"
        case homenumberprev = "home_number_prev"
        case infotext = "info_text"
        case infoworktime = "info_worktime"
        case infobankbik = "info_bank_bik"
        case infobankunp = "info_bank_unp"
        case gpsx = "GPS_X"
        case gpsy = "GPS_Y"
        case belnumberschet = "bel_number_schet"
        case foreignnumberschet = "foreign_number_schet"
        case phoneinfo = "phone_info"
        case info_weekend1_day = "info_weekend1_day"
        case info_weekend2_day = "info_weekend2_day"
        case info_weekend3_day = "info_weekend3_day"
        case info_weekend4_day = "info_weekend4_day"
        case info_weekend5_day = "info_weekend5_day"
        case info_weekend6_day = "info_weekend6_day"
        case info_weekend7_day = "info_weekend7_day"
        case info_weekend1_time = "info_weekend1_time"
        case info_weekend2_time = "info_weekend2_time"
        case info_weekend3_time = "info_weekend3_time"
        case info_weekend4_time = "info_weekend4_time"
        case info_weekend5_time = "info_weekend5_time"
        case info_weekend6_time = "info_weekend6_time"
        case info_weekend7_time = "info_weekend7_time"
        case dop_num = "dop_num"
        case strahovanie_med_rashodov = "strahovanie_med_rashodov"
        case usl_broker = "usl_broker"
        case usl_buy_slitki = "usl_buy_slitki"
        case usl_card_internet = "usl_card_internet"
        case usl_cennie_bumagi = "usl_cennie_bumagi"
        case usl_check_dover_vnebanka = "usl_check_dover_vnebanka"
        case usl_cheki_gilie = "usl_cheki_gilie"
        case usl_cheki_imuschestvo = "usl_cheki_imuschestvo"
        case usl_club_barhat = "usl_club_barhat"
        case usl_club_kartblansh = "usl_club_kartblansh"
        case usl_club_ledi = "usl_club_ledi"
        case usl_club_nastart = "usl_club_nastart"
        case usl_club_persona = "usl_club_persona"
        case usl_club_schodry = "case usl_club_schodry"
        case usl_coins_exchange = "usl_coins_exchange"
        case usl_depositariy = "usl_depositariy"
        case usl_dep_doverennosti = "usl_dep_doverennosti"
        case usl_dep_scheta = "usl_dep_scheta"
        case usl_dep_viplati = "usl_dep_viplati"
        case usl_docObligac_belarusbank = "usl_docObligac_belarusbank"
        case usl_dover_upr = "usl_dover_upr"
        case usl_dover_upr_gos = "usl_dover_upr_gos"
        case usl_drag_metal = "usl_drag_metal"
        case usl_ibank = "usl_ibank"
        case usl_inkasso_priem = "usl_inkasso_priem"
        case usl_inkasso_priem_deneg_bel = "usl_inkasso_priem_deneg_bel"
        case usl_inkasso_vyplata = "usl_inkasso_vyplata"
        case usl_int_cards = "usl_int_cards"
        case usl_izbiz_scheta_operacii = "usl_izbiz_scheta_operacii"
        case usl_izbiz_scheta_otkr = "usl_izbiz_scheta_otkr"
        case usl_kamni_brill = "usl_kamni_brill"
        case usl_kompleksny_product = "usl_kompleksny_product"
        case usl_konversiya_foreign_val = "usl_konversiya_foreign_val"
        case usl_loterei = "usl_loterei"
        case usl_mo_rb = "usl_mo_rb"
        case usl_operations_bezdokumentar_obligacii = "usl_operations_bezdokumentar_obligacii"
        case usl_operations_po_schetam_belpochta = "usl_operations_po_schetam_belpochta"
        case usl_operations_sber_sertif = "usl_operations_sber_sertif"
        case usl_perechislenie_po_rekvizitam_kartochki = "usl_perechislenie_po_rekvizitam_kartochki"
        case usl_plategi = "usl_plategi"
        case usl_podlinnost_banknot = "usl_podlinnost_banknot"
        case usl_pogashenie_documentar_obligacii = "usl_pogashenie_documentar_obligacii"
        case usl_popolnenieSchetaBezKart = "usl_popolnenieSchetaBezKart"
        case usl_popolnenieSchetaBynIspKarts = "usl_popolnenieSchetaBynIspKarts"
        case usl_popolnenieSchetaUsdIspKarts = "usl_popolnenieSchetaUsdIspKarts"
        case usl_pov = "usl_pov"
        case usl_priemDocPokupkaObl = "usl_priemDocPokupkaObl"
        case usl_priem_cennostei_na_hranenie = "usl_priem_cennostei_na_hranenie"
        case usl_priem_cennostej_na_hranenie = "usl_priem_cennostej_na_hranenie"
        case usl_priem_docs_fl_depozit_operations = "usl_priem_docs_fl_depozit_operations"
        case usl_priem_docs_vidacha_sopr_lgot_ipotech = "usl_priem_docs_vidacha_sopr_lgot_ipotech"
        case usl_priem_doc_na_kredits_magnit = "usl_priem_doc_na_kredits_magnit"
        case usl_priem_doc_na_kredits_overdrafts = "usl_priem_doc_na_kredits_overdrafts"
        case usl_priem_doc_na_lizing = "usl_priem_doc_na_lizing"
        case usl_priem_inkasso = "usl_priem_inkasso"
        case usl_priem_obl_mf = "usl_priem_obl_mf"
        case usl_priem_platejei_byn_ip = "usl_priem_platejei_byn_ip"
        case usl_priem_platejei_eur_ip = "usl_priem_platejei_eur_ip"
        case usl_priem_zayvleniy_obsluzhivanie_derzhatelej = "usl_priem_zayvleniy_obsluzhivanie_derzhatelej"
        case usl_prodaga_monet = "usl_prodaga_monet"
        case usl_pupil_card = "usl_pupil_card"
        case usl_razmen_foreign_val = "usl_razmen_foreign_val"
        case usl_razm_prodazha_documentar_obligacii = "usl_razm_prodazha_documentar_obligacii"
        case usl_rb_card = "usl_rb_card"
        case usl_registration_val_dogovor = "usl_registration_val_dogovor"
        case usl_return_BynIspKarts = "usl_return_BynIspKarts"
        case usl_return_UsdIspKarts = "usl_return_UsdIspKarts"
        case usl_rko = "usl_rko"
        case usl_seif = "usl_seif"
        case usl_strahovanie_avto = "usl_strahovanie_avto"
        case usl_strahovanie_avto_pogran = "usl_strahovanie_avto_pogran"
        case usl_strahovanie_detei = "usl_strahovanie_detei"
        case usl_strahovanie_dohod_pod_zaschitoy = "usl_strahovanie_dohod_pod_zaschitoy"
        case usl_strahovanie_express = "usl_strahovanie_express"
        case usl_strahovanie_green_karta = "usl_strahovanie_green_karta"
        case usl_strahovanie_home = "usl_strahovanie_home"
        case usl_strahovanie_kartochki = "usl_strahovanie_kartochki"
        case usl_strahovanie_kasko = "usl_strahovanie_kasko"
        case usl_strahovanie_komplex = "usl_strahovanie_komplex"
        case usl_strahovanie_medicine_nerezident = "usl_strahovanie_medicine_nerezident"
        case usl_strahovanie_perevozki = "usl_strahovanie_perevozki"
        case usl_strahovanie_s_zabotoi_o_blizkih = "usl_strahovanie_s_zabotoi_o_blizkih"
        case usl_strahovanie_timeAbroad = "usl_strahovanie_timeAbroad"
        case usl_strahovanie_zashhita_ot_kleshha = "usl_strahovanie_zashhita_ot_kleshha"
        case usl_strahovka_site = "usl_strahovka_site"
        case usl_striz = "usl_striz"
        case usl_stroysber = "usl_stroysber"
        case usl_stroysber_new = "usl_stroysber_new"
        case usl_subsidiya_scheta = "usl_subsidiya_scheta"
        case usl_swift = "usl_swift"
        case usl_vklad = "usl_vklad"
        case usl_vozvrat_nds = "usl_vozvrat_nds"
        case usl_vydacha_nal_v_banke = "usl_vydacha_nal_v_banke"
        case usl_vydacha_vypiski = "usl_vydacha_vypiski"
        case usl_vypllata_bel_rub = "usl_vypllata_bel_rub"
        case usl_vzk = "usl_vzk"
        case usl_wu = "usl_wu"
        case usl_plategi_all = "usl_plategi_all"
        case usl_plategi_in_foreign_val = "usl_plategi_in_foreign_val"
        case usl_plategi_za_proezd_v_polzu_banka = "usl_plategi_za_proezd_v_polzu_banka"
        case usl_plategi_minus_mobi = "usl_plategi_minus_mobi"
        case usl_plategi_minus_internet = "usl_plategi_minus_internet"
        case usl_plategi_minus_mobi_internet_full = "usl_plategi_minus_mobi_internet_full"
        case usl_plategi_nal_minus_krome_kredit = "usl_plategi_nal_minus_krome_kredit"
    }
    
    
    init(from decoder: Decoder) throws {
        let filialContainer = try decoder.container(keyedBy: FilialCodingKeys.self)
        
        filial_id = try filialContainer.decode(String.self, forKey: .filialid)
        sap_id = try filialContainer.decode(String.self, forKey: .sapid)
        filial_name = try filialContainer.decode(String.self, forKey: .filialname)
        filial_num = try filialContainer.decode(String.self, forKey: .filialnum)
        cbu_num = try filialContainer.decode(String.self, forKey: .cbunum)
        otd_num = try filialContainer.decode(String.self, forKey: .otdnum)
        name_type = try filialContainer.decode(String.self, forKey: .nametype)
        name = try filialContainer.decode(String.self, forKey: .name)
        street_type = try filialContainer.decode(String.self, forKey: .streettype)
        street = try filialContainer.decode(String.self, forKey: .street)
        home_number = try filialContainer.decode(String.self, forKey: .homenumber)
        name_type_prev = try filialContainer.decode(String.self, forKey: .nametypeprev)
        name_prev = try filialContainer.decode(String.self, forKey: .nameprev)
        street_type_prev = try filialContainer.decode(String.self, forKey: .streettypeprev)
        street_prev = try filialContainer.decode(String.self, forKey: .streetprev)
        home_number_prev = try filialContainer.decode(String.self, forKey: .homenumberprev)
        info_text = try filialContainer.decode(String.self, forKey: .infotext)
        info_worktime = try filialContainer.decode(String.self, forKey: .infoworktime)
        info_bank_bik = try filialContainer.decode(String.self, forKey: .infobankbik)
        info_bank_unp = try filialContainer.decode(String.self, forKey: .infobankunp)
        GPS_X = try filialContainer.decode(String.self, forKey: .gpsx)
        GPS_Y = try filialContainer.decode(String.self, forKey: .gpsy)
        bel_number_schet = try filialContainer.decode(String.self, forKey: .belnumberschet)
        foreign_number_schet = try filialContainer.decode(String.self, forKey: .foreignnumberschet)
        phone_info = try filialContainer.decode(String.self, forKey: .phoneinfo)
        info_weekend1_day = try filialContainer.decode(String.self, forKey: .info_weekend1_day)
        info_weekend2_day = try filialContainer.decode(String.self, forKey: .info_weekend2_day)
        info_weekend3_day = try filialContainer.decode(String.self, forKey: .info_weekend3_day)
        info_weekend4_day = try filialContainer.decode(String.self, forKey: .info_weekend4_day)
        info_weekend5_day = try filialContainer.decode(String.self, forKey: .info_weekend5_day)
        info_weekend6_day = try filialContainer.decode(String.self, forKey: .info_weekend6_day)
        info_weekend7_day = try filialContainer.decode(String.self, forKey: .info_weekend7_day)
        info_weekend1_time = try filialContainer.decode(String.self, forKey: .info_weekend1_time)
        info_weekend2_time = try filialContainer.decode(String.self, forKey: .info_weekend2_time)
        info_weekend3_time = try filialContainer.decode(String.self, forKey: .info_weekend3_time)
        info_weekend4_time = try filialContainer.decode(String.self, forKey: .info_weekend4_time)
        info_weekend5_time = try filialContainer.decode(String.self, forKey: .info_weekend5_time)
        info_weekend6_time = try filialContainer.decode(String.self, forKey: .info_weekend6_time)
        info_weekend7_time = try filialContainer.decode(String.self, forKey: .info_weekend7_time)
        dop_num = try filialContainer.decode(String.self, forKey: .dop_num)
        strahovanie_med_rashodov = try filialContainer.decode(String.self, forKey: .strahovanie_med_rashodov)
        usl_broker = try filialContainer.decode(String.self, forKey: .usl_broker)
        usl_buy_slitki = try filialContainer.decode(String.self, forKey: .usl_buy_slitki)
        usl_card_internet = try filialContainer.decode(String.self, forKey: .usl_card_internet)
        usl_cennie_bumagi = try filialContainer.decode(String.self, forKey: .usl_cennie_bumagi)
        usl_check_dover_vnebanka = try filialContainer.decode(String.self, forKey: .usl_check_dover_vnebanka)
        usl_cheki_gilie = try filialContainer.decode(String.self, forKey: .usl_cheki_gilie)
        usl_cheki_imuschestvo = try filialContainer.decode(String.self, forKey: .usl_cheki_imuschestvo)
        usl_club_barhat = try filialContainer.decode(String.self, forKey: .usl_club_barhat)
        usl_club_kartblansh = try filialContainer.decode(String.self, forKey: .usl_club_kartblansh)
        usl_club_ledi = try filialContainer.decode(String.self, forKey: .usl_club_ledi)
        usl_club_nastart = try filialContainer.decode(String.self, forKey: .usl_club_nastart)
        usl_club_persona = try filialContainer.decode(String.self, forKey: .usl_club_persona)
        usl_club_schodry = try filialContainer.decode(String.self, forKey: .usl_club_schodry)
        usl_coins_exchange = try filialContainer.decode(String.self, forKey: .usl_coins_exchange)
        usl_depositariy = try filialContainer.decode(String.self, forKey: .usl_depositariy)
        usl_dep_doverennosti = try filialContainer.decode(String.self, forKey: .usl_dep_doverennosti)
        usl_dep_scheta = try filialContainer.decode(String.self, forKey: .usl_dep_scheta)
        usl_dep_viplati = try filialContainer.decode(String.self, forKey: .usl_dep_viplati)
        usl_docObligac_belarusbank = try filialContainer.decode(String.self, forKey: .usl_docObligac_belarusbank)
        usl_dover_upr = try filialContainer.decode(String.self, forKey: .usl_dover_upr)
        usl_dover_upr_gos = try filialContainer.decode(String.self, forKey: .usl_dover_upr_gos)
        usl_drag_metal = try filialContainer.decode(String.self, forKey: .usl_drag_metal)
        usl_ibank = try filialContainer.decode(String.self, forKey: .usl_ibank)
        usl_inkasso_priem = try filialContainer.decode(String.self, forKey: .usl_inkasso_priem)
        usl_inkasso_priem_deneg_bel = try filialContainer.decode(String.self, forKey: .usl_inkasso_priem_deneg_bel)
        usl_inkasso_vyplata = try filialContainer.decode(String.self, forKey: .usl_inkasso_vyplata)
        usl_int_cards = try filialContainer.decode(String.self, forKey: .usl_int_cards)
        usl_izbiz_scheta_operacii = try filialContainer.decode(String.self, forKey: .usl_izbiz_scheta_operacii)
        usl_izbiz_scheta_otkr = try filialContainer.decode(String.self, forKey: .usl_izbiz_scheta_otkr)
        usl_kamni_brill = try filialContainer.decode(String.self, forKey: .usl_kamni_brill)
        usl_kompleksny_product = try filialContainer.decode(String.self, forKey: .usl_kompleksny_product)
        usl_konversiya_foreign_val = try filialContainer.decode(String.self, forKey: .usl_konversiya_foreign_val)
        usl_loterei = try filialContainer.decode(String.self, forKey: .usl_loterei)
        usl_mo_rb = try filialContainer.decode(String.self, forKey: .usl_mo_rb)
        usl_operations_bezdokumentar_obligacii = try filialContainer.decode(String.self, forKey: .usl_operations_bezdokumentar_obligacii)
        usl_operations_po_schetam_belpochta = try filialContainer.decode(String.self, forKey: .usl_operations_po_schetam_belpochta)
        usl_operations_sber_sertif = try filialContainer.decode(String.self, forKey: .usl_operations_sber_sertif)
        usl_perechislenie_po_rekvizitam_kartochki = try filialContainer.decode(String.self, forKey: .usl_perechislenie_po_rekvizitam_kartochki)
        usl_plategi = try filialContainer.decode(String.self, forKey: .usl_plategi)
        usl_podlinnost_banknot = try filialContainer.decode(String.self, forKey: .usl_podlinnost_banknot)
        usl_pogashenie_documentar_obligacii = try filialContainer.decode(String.self, forKey: .usl_pogashenie_documentar_obligacii)
        usl_popolnenieSchetaBezKart = try filialContainer.decode(String.self, forKey: .usl_popolnenieSchetaBezKart)
        usl_popolnenieSchetaBynIspKarts = try filialContainer.decode(String.self, forKey: .usl_popolnenieSchetaBynIspKarts)
        usl_popolnenieSchetaUsdIspKarts = try filialContainer.decode(String.self, forKey: .usl_popolnenieSchetaUsdIspKarts)
        usl_pov = try filialContainer.decode(String.self, forKey: .usl_pov)
        usl_priemDocPokupkaObl = try filialContainer.decode(String.self, forKey: .usl_priemDocPokupkaObl)
        usl_priem_cennostei_na_hranenie = try filialContainer.decode(String.self, forKey: .usl_priem_cennostei_na_hranenie)
        usl_priem_cennostej_na_hranenie = try filialContainer.decode(String.self, forKey: .usl_priem_cennostej_na_hranenie)
        usl_priem_docs_fl_depozit_operations = try filialContainer.decode(String.self, forKey: .usl_priem_docs_fl_depozit_operations)
        usl_priem_docs_vidacha_sopr_lgot_ipotech = try filialContainer.decode(String.self, forKey: .usl_priem_docs_vidacha_sopr_lgot_ipotech)
        usl_priem_doc_na_kredits_magnit = try filialContainer.decode(String.self, forKey: .usl_priem_doc_na_kredits_magnit)
        usl_priem_doc_na_kredits_overdrafts = try filialContainer.decode(String.self, forKey: .usl_priem_doc_na_kredits_overdrafts)
        usl_priem_doc_na_lizing = try filialContainer.decode(String.self, forKey: .usl_priem_doc_na_lizing)
        usl_priem_inkasso = try filialContainer.decode(String.self, forKey: .usl_priem_inkasso)
        usl_priem_obl_mf = try filialContainer.decode(String.self, forKey: .usl_priem_obl_mf)
        usl_priem_platejei_byn_ip = try filialContainer.decode(String.self, forKey: .usl_priem_platejei_byn_ip)
        usl_priem_platejei_eur_ip = try filialContainer.decode(String.self, forKey: .usl_priem_platejei_eur_ip)
        usl_priem_zayvleniy_obsluzhivanie_derzhatelej = try filialContainer.decode(String.self, forKey: .usl_priem_zayvleniy_obsluzhivanie_derzhatelej)
        usl_prodaga_monet = try filialContainer.decode(String.self, forKey: .usl_prodaga_monet)
        usl_pupil_card = try filialContainer.decode(String.self, forKey: .usl_pupil_card)
        usl_razmen_foreign_val = try filialContainer.decode(String.self, forKey: .usl_razmen_foreign_val)
        usl_razm_prodazha_documentar_obligacii = try filialContainer.decode(String.self, forKey: .usl_razm_prodazha_documentar_obligacii)
        usl_rb_card = try filialContainer.decode(String.self, forKey: .usl_rb_card)
        usl_registration_val_dogovor = try filialContainer.decode(String.self, forKey: .usl_registration_val_dogovor)
        usl_return_BynIspKarts = try filialContainer.decode(String.self, forKey: .usl_return_BynIspKarts)
        usl_return_UsdIspKarts = try filialContainer.decode(String.self, forKey: .usl_return_UsdIspKarts)
        usl_rko = try filialContainer.decode(String.self, forKey: .usl_rko)
        usl_seif = try filialContainer.decode(String.self, forKey: .usl_seif)
        usl_strahovanie_avto = try filialContainer.decode(String.self, forKey: .usl_strahovanie_avto)
        usl_strahovanie_avto_pogran = try filialContainer.decode(String.self, forKey: .usl_strahovanie_avto_pogran)
        usl_strahovanie_detei = try filialContainer.decode(String.self, forKey: .usl_strahovanie_detei)
        usl_strahovanie_dohod_pod_zaschitoy = try filialContainer.decode(String.self, forKey: .usl_strahovanie_dohod_pod_zaschitoy)
        usl_strahovanie_express = try filialContainer.decode(String.self, forKey: .usl_strahovanie_express)
        usl_strahovanie_green_karta = try filialContainer.decode(String.self, forKey: .usl_strahovanie_green_karta)
        usl_strahovanie_home = try filialContainer.decode(String.self, forKey: .usl_strahovanie_home)
        usl_strahovanie_kartochki = try filialContainer.decode(String.self, forKey: .usl_strahovanie_kartochki)
        usl_strahovanie_kasko = try filialContainer.decode(String.self, forKey: .usl_strahovanie_kasko)
        usl_strahovanie_komplex = try filialContainer.decode(String.self, forKey: .usl_strahovanie_komplex)
        usl_strahovanie_medicine_nerezident = try filialContainer.decode(String.self, forKey: .usl_strahovanie_medicine_nerezident)
        usl_strahovanie_perevozki = try filialContainer.decode(String.self, forKey: .usl_strahovanie_perevozki)
        usl_strahovanie_s_zabotoi_o_blizkih = try filialContainer.decode(String.self, forKey: .usl_strahovanie_s_zabotoi_o_blizkih)
        usl_strahovanie_timeAbroad = try filialContainer.decode(String.self, forKey: .usl_strahovanie_timeAbroad)
        usl_strahovanie_zashhita_ot_kleshha = try filialContainer.decode(String.self, forKey: .usl_strahovanie_zashhita_ot_kleshha)
        usl_strahovka_site = try filialContainer.decode(String.self, forKey: .usl_strahovka_site)
        usl_striz = try filialContainer.decode(String.self, forKey: .usl_striz)
        usl_stroysber = try filialContainer.decode(String.self, forKey: .usl_stroysber)
        usl_stroysber_new = try filialContainer.decode(String.self, forKey: .usl_stroysber_new)
        usl_subsidiya_scheta = try filialContainer.decode(String.self, forKey: .usl_subsidiya_scheta)
        usl_swift = try filialContainer.decode(String.self, forKey: .usl_swift)
        usl_vklad = try filialContainer.decode(String.self, forKey: .usl_vklad)
        usl_vozvrat_nds = try filialContainer.decode(String.self, forKey: .usl_vozvrat_nds)
        usl_vydacha_nal_v_banke = try filialContainer.decode(String.self, forKey: .usl_vydacha_nal_v_banke)
        usl_vydacha_vypiski = try filialContainer.decode(String.self, forKey: .usl_vydacha_vypiski)
        usl_vypllata_bel_rub = try filialContainer.decode(String.self, forKey: .usl_vypllata_bel_rub)
        usl_vzk = try filialContainer.decode(String.self, forKey: .usl_vzk)
        usl_wu = try filialContainer.decode(String.self, forKey: .usl_wu)
        usl_plategi_all = try filialContainer.decode(String.self, forKey: .usl_plategi_all)
        usl_plategi_in_foreign_val = try filialContainer.decode(String.self, forKey: .usl_plategi_in_foreign_val)
        usl_plategi_za_proezd_v_polzu_banka = try filialContainer.decode(String.self, forKey: .usl_plategi_za_proezd_v_polzu_banka)
        usl_plategi_minus_mobi = try filialContainer.decode(String.self, forKey: .usl_plategi_minus_mobi)
        usl_plategi_minus_internet = try filialContainer.decode(String.self, forKey: .usl_plategi_minus_internet)
        usl_plategi_minus_mobi_internet_full = try filialContainer.decode(String.self, forKey: .usl_plategi_minus_mobi_internet_full)
        usl_plategi_nal_minus_krome_kredit = try filialContainer.decode(String.self, forKey: .usl_plategi_nal_minus_krome_kredit)
    }
}

