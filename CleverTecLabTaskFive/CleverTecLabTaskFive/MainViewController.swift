//
//  MainViewController.swift
//  CleverTecLabProject
//
//  Created by Zakhar Klochkov on 3.10.21.
//

import UIKit
import MapKit

class MainViewController: UIViewController, MKMapViewDelegate {
    
    private var networkManager: NetworkManager!
    private var langCenter = 52.42505
    private var longCenter = 31.01407
    private let distanceSpan: CLLocationDegrees = 10000
    private let annotation = MKPointAnnotation()
    private var location: CLLocationCoordinate2D = CLLocationCoordinate2D()
    private var pointArr: [Point] = [Point]()
    
    private lazy var mapView: MKMapView = {
        var mp = MKMapView()
        mp.mapType = MKMapType.standard
        mp.isZoomEnabled = true
        mp.isScrollEnabled = true
        
        return mp
    }()
    
    init(networkManager: NetworkManager) {
        super.init(nibName: nil, bundle: nil)
        self.networkManager = networkManager
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let leftMargin:CGFloat = 10
        let topMargin:CGFloat = 10
        
        mapView.frame = CGRect(x: leftMargin, y: topMargin, width: view.frame.size.width, height: view.frame.size.height)
        mapView.center = view.center
        mapView.delegate = self
        
        view.addSubview(mapView)
        startGetAllTypePointInfo()
        view.backgroundColor = .green
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
        //            self.setCenterMap(lang: self.langCenter, long: self.longCenter)
        //        }
    } 
    
    fileprivate func startGetAllTypePointInfo() {
        DispatchQueue.main.async {
            self.networkManager.getAllATM() {
                data, error in
                if let error = error {
                    print(error)
                }
                if let data = data {
                    for pos in data {
                        self.pointArr.append(Point(type: "ATM",
                                                   x: pos.gps_x,
                                                   y: pos.gps_y,
                                                   distanceToCenter: self.getDistanceToCenter(x: pos.gps_x, y: pos.gps_y)))
                    }
                    self.addPointToMap(arrayPoints: self.pointArr)
                }
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.networkManager.getAllInfobox() {
                data, error in
                if let error = error {
                    print(error)
                }
                if let data = data {
                    for pos in data {
                        self.pointArr.append(Point(type: "Infobox",
                                                   x: pos.gps_x,
                                                   y: pos.gps_y,
                                                   distanceToCenter: self.getDistanceToCenter(x: pos.gps_x, y: pos.gps_y)))
                    }
                    self.addPointToMap(arrayPoints: self.pointArr.sorted(by: {$0.distanceToCenter < $1.distanceToCenter}))
                }
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.networkManager.getAllFilial() {
                data, error in
                if let error = error {
                    print(error)
                }
                if let data = data {
                    for pos in data {
                        self.pointArr.append(Point(type: "Filial",
                                                   x: pos.GPS_X,
                                                   y: pos.GPS_Y,
                                                   distanceToCenter: self.getDistanceToCenter(x: pos.GPS_X, y: pos.GPS_Y)))
                    }
                    self.addPointToMap(arrayPoints: self.pointArr.sorted(by: {$0.distanceToCenter < $1.distanceToCenter}))
                }
            }
        }
    }
    
    func addPointToMap(arrayPoints: [Point]){
        for point in getNArrayElement(array: arrayPoints.sorted(by: {$0.distanceToCenter < $1.distanceToCenter}), count: 10) {
            guard let lang = Double(point.x) else { return }
            guard let long = Double(point.y) else { return }
            self.addAnnotation(lang: lang.rounded(toPlaces: 5), long: long.rounded(toPlaces: 5), title: point.type)
        }
    }
    
    func addInfoboxPoints(arrayPoints: [Infobox]){
        for atm in arrayPoints {
            guard let lang = Double(atm.gps_x) else { return }
            guard let long = Double(atm.gps_y) else { return }
            self.addAnnotation(lang: lang.rounded(toPlaces: 5), long: long.rounded(toPlaces: 5), title: "Infobox")
        }
    }
    
    func setCenterMap(lang: Double, long: Double){
        location = CLLocationCoordinate2DMake(lang, long)
        let coordinateRegion = MKCoordinateRegion(center: location, latitudinalMeters: distanceSpan, longitudinalMeters: distanceSpan)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func addAnnotation(lang: CLLocationDegrees, long: CLLocationDegrees, title: String) {
        let pin = MKPlacemark(coordinate: CLLocationCoordinate2DMake(lang, long))
        let coordinateRegion = MKCoordinateRegion(center: pin.coordinate, latitudinalMeters: distanceSpan, longitudinalMeters: distanceSpan)
        mapView.setRegion(coordinateRegion, animated: true)
        annotation.title = title
        mapView.addAnnotation(annotation)
        mapView.addAnnotation(pin)
    }
    
    func getDistanceToCenter(x: String, y: String) -> Double {
        
        return sqrt(pow(Double(x) ?? 0.0-langCenter, 2) + pow(Double(y) ?? 0.0-longCenter, 2))
    }
    
    func getNArrayElement(array: [Point], count: Int) -> [Point] {
        var arrTemp:[Point] = [Point]()
        for item in array[0..<count] {
            arrTemp.append(item)
        }
        return arrTemp
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

