//
//  NetworkRouterCompletion.swift
//  CleverTecLabTaskFive
//
//  Created by Zakhar Klochkov on 14.10.21.
//

import Foundation

public typealias NetworkRouterCompletion = (_ data: Data?, _ response: URLResponse?,_ error: Error?) -> ()

protocol NetworkRouter: AnyObject {
    associatedtype EndPoint: EndPointType
    func request(_ route: EndPoint, completion: @escaping NetworkRouterCompletion)
    func cancel()
}
